from django.contrib import admin

from .models import Treads, Comment

admin.site.register(Treads)
admin.site.register(Comment)
# Register your models here.
