from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils import timezone
from django import forms
import datetime


@python_2_unicode_compatible
class Treads(models.Model):
    title_text = models.CharField(max_length=2000)
    tag_text = models.CharField(max_length=30)
    preview_text = models.TextField()
    tread_text = models.TextField()
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.tread_text

    def tread_tag(self):
        return self.tag_text

    def tread_title(self):
        return self.title_text

    def tread_preview(self):
        return self.preview_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now


@python_2_unicode_compatible
class Comment(models.Model):
    author = models.CharField(max_length=30)
    tread_ids = forms.CharField(max_length=300)
    comment_text = models.TextField()
    pub_date = models.DateTimeField(auto_now=True)


    def __str__(self):
        return self.comment_text

    def treadid(self):
        return self.tread_ids

    def comment_author(self):
        return self.author

    def was_published_recently(self):
        return self.pub_date
