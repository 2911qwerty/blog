from django import forms
from .models import Treads


class RegForm(forms.Form):
    username = forms.CharField(max_length=30)
    email = forms.EmailField()


class AuthForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(max_length=30)


class UsersComment(forms.Form):
    #author = forms.CharField(max_length=30)
    comment = forms.TextInput()
    #tread_id = forms.CharField(max_length=300)



class PostingForm(forms.ModelForm):
    class Meta:
        model = Treads
        fields = ('title_text', 'tread_text')
