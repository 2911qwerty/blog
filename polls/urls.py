from django.conf.urls import patterns, include, url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<post_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^register/$', views.reg, name='reg'),
    url(r'^logout/$', views.users_logout, name='logout'),
    #url(r"^comments/", include("django.contrib.comments.urls")),

]