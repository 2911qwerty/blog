# -*- coding: UTF-8 -*-
from .models import Treads, Comment
from .forms import RegForm, AuthForm, UsersComment
from django.shortcuts import get_object_or_404, render, HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.core.mail import send_mail, BadHeaderError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import random, string

def rand_pwd(pwd=''):
    i = 0
    while i < 8:
        pwd += random.choice(string.printable[0:94])
        i += 1
    return pwd


def reg(request):
    if request.method == 'POST':
        form = RegForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = rand_pwd()
            email = request.POST['email']
            User.objects.create_user(username=username, email=email, password=password)
            '''recipient_list=[]
            recipient_list.append(email)
            subject = u'Регистрация'
            message = u'Добро пожаловать! \n Учетная запись: %s \n Пароль: %s ' % (username, password)
            try:
                send_mail(subject=subject, message=message, from_email='test@test.ru', recipient_list=recipient_list)
            except BadHeaderError:  # Защита от уязвимости
                return HttpResponse('Invalid header found')'''
            return render(request, 'polls/success.html', {'username': username, 'email': email, 'password': password})
    else:
        form = RegForm()
        return render(request, 'polls/register.html', {'form': form})


def user_login(request, latest_treads_list):
    if request.method == 'POST':
        form = AuthForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Successful!')
                else:
                    return HttpResponse('login is blocked')
            else:
                return HttpResponse('disabled account')
        else:
            return HttpResponse('correct mistakes')
    else:
        form = AuthForm()
        return render(request, 'polls/index.html', {'latest_treads_list': latest_treads_list, 'form': form})


def news_feed(request):
    if True:
        latest_treads_list = Treads.objects.order_by('title_text')
    else:
        latest_treads_list = Treads.filter(tread_tag=True).objects.order_by('-pub_date')
    paginator = Paginator(latest_treads_list, 5)
    page = request.GET.get('page')
    try:
        latest_treads_list = paginator.page(page)
    except PageNotAnInteger:
        latest_treads_list = paginator.page(1)
    except EmptyPage:
        latest_treads_list = paginator.page(paginator.num_pages)
    return latest_treads_list


def index(request):

    latest_treads_list = news_feed(request)

    if request.user.is_authenticated():
        name = request.user.username
        return render(request, 'polls/index.html', {'latest_treads_list': latest_treads_list, 'name': name})

    else:
        your_login = user_login(request, latest_treads_list)

    return your_login


def users_logout(request):
    logout(request)
    return render(request, 'polls/logout.html')


def new_comment(request, post_id):
    if request.method == 'POST':
        form = UsersComment(request.POST)
        if form.is_valid():
            form.author = request.user.username
            form.comment = request.POST['comment']
            #form.tread_id = request.POST['post_id']
            form.save()
        else:
            form = UsersComment()

    return render(request, 'polls/detail.html', {'form': form})


def detail(request, post_id):
    tread = get_object_or_404(Treads, pk=post_id)
    #author = request.user.username
    #comments = get_object_or_404(Comment, pk=post_id)
    latest_comments_list = Comment.objects.order_by('-pub_date').all()
    latest_authors_list = Comment.objects.order_by('-pub_date').all()
    #nc = new_comment(request, post_id)
    context = {'tread': tread, 'latest_comments_list': latest_comments_list,
               'latest_authors_list': latest_authors_list}#, 'nc': nc}
    return render(request, 'polls/detail.html', context)

#.filter(tread_id=post_id)





'''def detail(request, post_id):
    latest_comment_list = Treads.objects.order_by('-pub_date')
    return render(request, 'polls/comments.html', {'latest_comment_list': latest_comment_list})'''